using WebApi.Services.Exceptions;

namespace WebApi.Services.BaseHelpers;



public static class Guard
{
    public static void NotNull(object? obj)
    {
        if (obj == null)
            throw new ArgumentIsNullException();
    }
}
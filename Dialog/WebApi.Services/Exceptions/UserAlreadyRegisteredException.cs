namespace WebApi.Services.Exceptions;

public class UserAlreadyRegisteredException : Exception
{
    public UserAlreadyRegisteredException(): base("Пользователь с таким email уже зарегистрирован!") 
    {
    }
    
    public UserAlreadyRegisteredException(string message) 
        : base(message)
    {
    }
    
    public UserAlreadyRegisteredException(string message, Exception innerExeption) 
        : base(message, innerExeption)
    {
    }
}
namespace WebApi.Services.Exceptions;

public class ArgumentIsNullException : Exception
{
    public ArgumentIsNullException(): base("Аргумент равен null") 
    {
    }
    
    public ArgumentIsNullException(string message) 
        : base(message)
    {
    }
    
    public ArgumentIsNullException(string message, Exception innerExeption) 
        : base(message, innerExeption)
    {
    }
}
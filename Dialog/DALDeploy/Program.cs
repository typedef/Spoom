﻿// See https://aka.ms/new-console-template for more information

using DAL;
using DAL.Factory;

namespace DALDeploy // Note: actual namespace depends on the project name.
{
    internal class EntryPoint
    {
        static async Task Main(string[] args)
        {
            var readedLine = Console.ReadLine();
            if (readedLine == "yes" || readedLine == "y")
            {
                await MongoDbKeeper.Deploy();
            }
        }
    }
}



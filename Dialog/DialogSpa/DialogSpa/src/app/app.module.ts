// Ng modules
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';

// Ng Mat Modules
import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import {MatMenuModule} from '@angular/material/menu';

// Modules
import {AppRoutingModule} from './app-routing.module';

// My components
import {AppComponent} from './app.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {LoginBarComponent} from './components/login-bar/login-bar.component';

// Services
import {AuthService} from './core/services/app/auth.service';
import {CacheService} from "./core/services/app/cache.service";
import {GuardService} from "./core/services/security/guard.service";
import {HttpRequestInterceptor} from "./interceptors/http-request-interceptor";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    LoginBarComponent,
  ],
  imports: [
    // Ng
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,

    // Ng Mat
    MatMenuModule,

    // Ng Mat Modules
    MatInputModule,
    MatDialogModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpRequestInterceptor,
      multi: true,
    },

    // Ng
    HttpClient,

    // My
    AuthService,
    CacheService,
    GuardService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

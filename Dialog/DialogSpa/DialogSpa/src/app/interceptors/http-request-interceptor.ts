import {Injectable} from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HTTP_INTERCEPTORS,
  HttpHeaders
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CacheService} from "../core/services/app/cache.service";
import {UserViewModel} from "../core/view-models";
import * as http from "http";

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {

  constructor(private _cacheService: CacheService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // let userRecordStr = this._cacheService.get("user");
    // if (!userRecordStr)
    //   return next.handle(request);
    //
    // let userViewModel: UserViewModel = JSON.parse(userRecordStr) as UserViewModel;
    // if (!userViewModel)
    //   return next.handle(request);
    //
    // let newRequest = request.clone({ setHeaders: { Authorization: `Bearer ${userViewModel.AccessToken}` } });
    // debugger;
    return next.handle(request);
  }
}

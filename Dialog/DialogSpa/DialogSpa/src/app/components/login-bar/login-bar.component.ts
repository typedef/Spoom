import {Component, Input, OnInit} from '@angular/core';
import {UserViewModel} from "../../core/view-models";

@Component({
  selector: 'login-bar-component',
  templateUrl: './login-bar.component.html',
  styleUrls: ['./login-bar.component.css']
})
export class LoginBarComponent implements OnInit {

  @Input() public user: UserViewModel;

  constructor() { }

  ngOnInit(): void {
  }

  public menuProccess(): void {
    console.log("Do nothing for now!");
  }

}

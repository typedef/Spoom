import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {AuthService} from "../../core/services/app/auth.service";
import {CacheService} from "../../core/services/app/cache.service";
import {LoginData} from "../login/login.component";
import {UserType} from "../../core/view-models";

export class RegisterData {
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  // TODO: We need implement dropdown && input
  public userType: UserType = UserType.Admin;
  public login: string;
  public password: string;
  public repeatedPassword: string;

  constructor(public dialogRef: MatDialogRef<RegisterComponent>,
              @Inject(MAT_DIALOG_DATA) public data: RegisterData,
              private _authService: AuthService,
              private _cacheService: CacheService) { }

  ngOnInit(): void {
  }

  public register(): void {

  }
}

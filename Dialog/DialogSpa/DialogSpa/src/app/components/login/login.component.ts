import {Component, Inject, OnInit} from '@angular/core';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA}
  from '@angular/material/dialog';
import {AuthService} from 'src/app/core/services/app/auth.service';
import {LoginRequestViewModel, UserViewModel} from 'src/app/core/view-models';
import {CacheService} from "../../core/services/app/cache.service";

export class LoginData {
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public login: string = '';
  public password: string = '';

  constructor(public dialogRef: MatDialogRef<LoginComponent>,
              @Inject(MAT_DIALOG_DATA) public data: LoginData,
              private _authService: AuthService,
              private _cacheService: CacheService
  ) {
  }

  ngOnInit(): void {
  }

  public sendLoginRequest(): void {
    console.log(`login: ${this.login} password: ${this.password}`);

    let authRequest: LoginRequestViewModel = new LoginRequestViewModel();
    authRequest.Login = this.login;
    authRequest.Password = this.password;

    let loginObserver = {
      next: (success: UserViewModel): void => {
        console.log(`Success: ${JSON.stringify(success)}`);
        this._cacheService.add("user", JSON.stringify(success));
        this.dialogRef.close();
      },
      error: (error: any): void => {
        console.error(error);
      }
    };

    this._authService.login(authRequest).subscribe(loginObserver);
  }

  private loginProccess(user: UserViewModel): void {
    this.dialogRef.close();


  }


}

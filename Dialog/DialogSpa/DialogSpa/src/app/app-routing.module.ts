import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {GuardService} from "./core/services/security/guard.service";

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./modules/all.modules')
      .then(m => m.AllModules),
    //canActivate: [GuardService]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

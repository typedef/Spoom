import {Component} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {LoginComponent, LoginData} from './components/login/login.component';
import {UserViewModel} from "./core/view-models";
import {AuthService} from "./core/services/app/auth.service";
import {CacheService} from "./core/services/app/cache.service";
import {RegisterComponent, RegisterData} from "./components/register/register.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public user: UserViewModel;
  public isLoggedIn: boolean = false;

  constructor(private _dialog: MatDialog,
              private _authService: AuthService,
              private _cacheService: CacheService) {
    this.getUserAndSetLoggerIn()
  }

  public onLoginClicked(): void {
    console.log("Not doing much on login clicked for now!");

    this._dialog.open(LoginComponent, {
      data: new LoginData()
    });

    this._dialog.afterAllClosed.subscribe({
      next: _ => {
        this.getUserAndSetLoggerIn();
      }
    });

  }

  public onRegisterClicked(): void {
    console.log("Not doing much on register clicked for now!");

    this._dialog.open(RegisterComponent, {
      data: new RegisterData()
    });

    this._dialog.afterAllClosed.subscribe({
      next: _ => {
      }
    });
  }

  /*
    NOTE: Dev
  */
  public dev_dropUser(): void {
    this._cacheService.update("user", null);
    // ctrl + f5
    window.location.href = window.location.href;
  }

  private getUser(): UserViewModel {
    return JSON.parse(this._cacheService.get("user")) as UserViewModel;
  }

  public dev_getUser(): UserViewModel {
    let u = this.getUser();
    console.log(u);
    return u || {};
  }

  public dev_checkAuth(): void {
    let userViewModel: UserViewModel = this.getUser();
    this._authService.checkAuth(userViewModel.AccessToken).subscribe({
      next: success => {
        console.log(success);
      },
      error: err => {
        console.error(err);
      }
    })
  }

  public dev_checkAuth2(): void {
    this._authService.checkAuth2().subscribe({
      next: success => {
        console.log(success);
      },
      error: err => {
        console.error(err);
      }
    })
  }

  /*
    NOTE: Private functions, mostly internal
  */
  private getUserAndSetLoggerIn(): void {
    this.user = this.getUserFromCache();
    this.setLoggedIn();
  }

  private getUserFromCache(): UserViewModel {
    let userJson: string = this._cacheService.get("user");
    let user: UserViewModel = JSON.parse(userJson) as UserViewModel;
    return user;
  }

  private setLoggedIn(): void {
    if ((this.user !== null) && (this.user !== undefined)) {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
    }
  }
}

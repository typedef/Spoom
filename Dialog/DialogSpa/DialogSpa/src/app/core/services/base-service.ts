import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

export enum EnvironmentType {
  Dev = 0,
  Test = 1,
  Prod = 2
}

@Injectable()
export class BaseService {
  public environment: EnvironmentType;
  public hostUrl: string;

  constructor(public httpClient: HttpClient) {
    this.environment = EnvironmentType.Dev;
    this.hostUrl = this.getHostUrlForEnvironmet(this.environment);
  }

  private getHostUrlForEnvironmet(type: EnvironmentType): string {
    if (type == EnvironmentType.Dev) {
      return "http://localhost:5148/api/";
    }

    console.error("Unknown environment!");
    return '';
  }
}

import {Injectable} from "@angular/core";
import {BaseService} from "../base-service";
import {LoginRequestViewModel, RegisterRequestViewModel, UserViewModel} from '../../view-models';
import {Observable} from "rxjs";
import {HttpHeaders} from "@angular/common/http";

@Injectable()
export class AuthService extends BaseService {

  public login(authReq: LoginRequestViewModel): Observable<UserViewModel> {
    return this.httpClient.post(this.hostUrl + "user/login", authReq) as Observable<UserViewModel>;
  }

  public register(registerReq: RegisterRequestViewModel): Observable<UserViewModel> {
    return this.httpClient.post(this.hostUrl + "user/register", registerReq) as Observable<UserViewModel>;
  }

  public checkAuth(accessToken: string): Observable<boolean> {
    let headers = { 'Authorization': 'Bearer ' + accessToken };
    let v = 'Bearer ' + accessToken;
    let h = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', v);
    console.log(h);
    //debugger;
    return this.httpClient.get(this.hostUrl + "user/check-access", {headers: h}) as Observable<boolean>;
  }
  public checkAuth2(): Observable<boolean> {
    return this.httpClient.get(this.hostUrl + "user/check-access2") as Observable<boolean>;
  }

}

import {Injectable} from "@angular/core";

@Injectable()
export class CacheService {
  
  // reference https://learn.javascript.ru/cookie
  public add(key: string, val: string, expiresInSeconds: number = 2*3600 /* 2 hours */): void {
    document.cookie = `${key}:${val}; max-age=${expiresInSeconds}`;
  }

  public get(cacheKey: string): any {
    let cachedVals: string[] = document.cookie.split(';');
    for (let i = 0; i < cachedVals.length; ++i) {
      let pair = cachedVals[i];
      let key = pair.split(":")[0].trim();
      let val = pair.substring(pair.indexOf(":") + 1, pair.length).trim();
      if (key === cacheKey) {
        return val;
      }
    }

    return null;
  }

  public update(cacheKey: string, cacheValue: any, expiresInSeconds: number = 2*3600 /* 2 hours */): void {
    document.cookie = `${cacheKey}:${cacheValue}; max-age=${expiresInSeconds}`;
  }
}

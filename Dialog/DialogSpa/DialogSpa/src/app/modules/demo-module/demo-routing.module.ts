import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DemoComponent} from './components/demo/demo.component';
import {GuardService} from "../../core/services/security/guard.service";

const routes: Routes = [
  {
    path: 'demo',
    component: DemoComponent,
    canActivate: [GuardService]
  },
  {
    path: 'new',
    component: DemoComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoRoutingModule {
}

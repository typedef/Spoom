import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { DemoComponent } from "./components/demo/demo.component";
import { DemoService } from './services/DemoService';
import { DemoRoutingModule } from './demo-routing.module';

@NgModule(
  {
    imports: [
      RouterModule,
      CommonModule,
      HttpClientModule,
      // application modules
      DemoRoutingModule,
    ],
    declarations: [
      // Declare components inside this module
      DemoComponent
    ],
    exports: [
      RouterModule,
      CommonModule,
      HttpClientModule,
      // application modules
      DemoRoutingModule,
    ],
    providers: [
      // Ng
      HttpClient,

      // My
      DemoService
    ]
  }
)
export class DemoModule { }

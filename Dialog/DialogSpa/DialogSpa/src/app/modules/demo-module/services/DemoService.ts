import {HttpClient, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/internal/Observable";

@Injectable()
export class DemoService {
  private _baseUrl: string = "https://localhost:7099/";
  private _controller: string = "api/home/"

  constructor(private _httpClient: HttpClient) {
  }

  public getVersion(): Observable<string> {
    let result = this._httpClient.get(
      this._baseUrl + this._controller + "home-get-version") as Observable<string>;
    return result;
  }

  public checkAccess(): Observable<boolean> {
    let params: HttpParams = new HttpParams();
    return this._httpClient.get("api/user/check-access") as Observable<boolean>;
  }
}

import { HttpClient, HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule } from '@angular/forms';
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";

import { AllModulesRouting } from "./all.modules-routing";

@NgModule(
  {
    imports: [
      // Ng
      RouterModule,
      FormsModule,
      HttpClientModule,

      // my modules
      AllModulesRouting,
    ],
    declarations: [
      // for components
    ],
    exports: [
      RouterModule,
      AllModulesRouting,
    ],
    providers: [
      // for services

      // Ng
      HttpClient,
    ]
  }
)
export class AllModules {

}

using Kernel.Types;

namespace Kernel.Models;

public class BookModel
{
    /// <summary>
    /// GUID
    /// </summary>
    public string? Id { get; set; }
    /// <summary>
    /// GUID
    /// </summary>
    public string? ProducerId { get; set; }
    public string? Name { get; set; }
    public int AgeLimit { get; set; }
    public DateTime? PublishingDate { get; set;}
    public int PagesCount { get; set; }
    public GenreType Genre { get; set; }
    
    public BookModel() {}
}
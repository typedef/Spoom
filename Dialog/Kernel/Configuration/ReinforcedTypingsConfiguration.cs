using System.Reflection;
using Reinforced.Typings.Fluent;

namespace Kernel.Data.ViewModels;

public static class ReinforcedTypingsConfiguration
{
    public static void Configure(ConfigurationBuilder builder)
    {
        builder.Global(c => c.UseModules());
    }
}
using Reinforced.Typings.Attributes;

namespace Kernel.Data.ViewModels.Auth;

[TsEnum]
public enum UserType
{
    Teacher = 0,
    Student,
    Admin
}
using Kernel.Configuration;

namespace Kernel.Data.ViewModels.Auth;

[Ts]
public class RegisterRequestViewModel
{
    public UserType? Type { get; set; }
    public string? Login { get; set; }
    public string? Password { get; set; }
}
using Kernel.Configuration;

namespace Kernel.Data.ViewModels.Auth;

[Ts]
public class UserViewModel
{
    public string? Login { get; set; }
    public string? Password { get; set; }
    public UserType? Type { get; set; }
    // Токен доступа хранится в памяти, при закрытии вкладки токен доступа будет потерян
    public string? AccessToken { get; set; }
    // Токен обновления токена доступа, хранить в куки (http only)
    public string? RefreshToken { get; set; }
}


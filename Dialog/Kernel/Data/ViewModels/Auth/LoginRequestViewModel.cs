using Kernel.Configuration;
using Reinforced.Typings.Attributes;

namespace Kernel.Data.ViewModels.Auth;

[Ts]
public class LoginRequestViewModel
{
    public string? Login { get; set; }
    public string? Password { get; set; }

    public LoginRequestViewModel(string login, string password)
    {
        Login = login;
        Password = password;
    }
}
namespace Kernel.Types;

public enum CompareFieldType
{
    Full = 0,
    Contains,
    StartsWith,
    EndsWith,
    Greater,
    Less
}
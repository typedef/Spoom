namespace Kernel.Types;

public enum GenreType
{
    Fantasy = 0,
    ScienceFiction,
    HistoricalFiction,
    RealisticFiction,
    FanFiction,
    Biography,
    ReferenceBook
}
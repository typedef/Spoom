using MongoDB.Driver;

namespace DAL.Context;

public class MongoContext : IMongoContext
{
    private readonly MongoClient _mongoClient;

    // ReSharper disable once ConvertConstructorToMemberInitializers
    public MongoContext()
    {
        _mongoClient = new MongoClient("mongodb://localhost:27017");
    }
    
    public IMongoDatabase GetDataBase(string name)
    {
        IMongoDatabase db = _mongoClient.GetDatabase(name);
        return db;
    }
    
}


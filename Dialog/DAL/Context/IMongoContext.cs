using MongoDB.Driver;

namespace DAL.Context;

public interface IMongoContext
{
    IMongoDatabase GetDataBase(string name);
}
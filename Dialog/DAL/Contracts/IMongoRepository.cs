using MongoDB.Bson;
using MongoDB.Driver;

namespace DAL.Contracts;

public interface IMongoRepository
{
    Task Create();
}
using System.Diagnostics.CodeAnalysis;
using DAL.Context;
using DAL.Contracts;
using DAL.Repository;
using Kernel.Models;
using MongoDB.Driver;

namespace DAL.Factory;

[SuppressMessage("ReSharper", "CollectionNeverQueried.Local")]
[SuppressMessage("ReSharper", "PrivateFieldCanBeConvertedToLocalVariable")]
public class MongoDbFactory
{
    private readonly IMongoContext _mongoContext;
    private readonly IMongoDatabase _miraSdb;
    private readonly Dictionary<string, IMongoRepository> _repositories;
    
    public MongoDbFactory()
    {
        _mongoContext = new MongoContext();
        _miraSdb = _mongoContext.GetDataBase("MiraSDB");

        _repositories = new Dictionary<string, IMongoRepository>();
    }

    /// <summary>
    /// Вызывать только для DALDeploy
    /// </summary>
    public async Task RecreateRepositories()
    {
        await RecreateCollection<BookModel>("Books", (collection) => new BookRepository(collection));
    }
    
    /// <summary>
    /// Вызывать во всех остальных случаях, чтобы проинициализировать Репозитории
    /// </summary>
    public void InitRepositories()
    {
        InitRepository<BookModel>("Books", (collection) => new BookRepository(collection));
    }

    public IMongoRepository GetRepository(string repositoryName)
    {
        return _repositories[repositoryName];
    }
    
    private async Task RecreateCollection<T>(string name, Func<IMongoCollection<T>, IMongoRepository> ctor)
    {
        await _miraSdb.DropCollectionAsync(name);
        await _miraSdb.CreateCollectionAsync(name);
        IMongoRepository repository = InitRepository<T>(name, ctor);
        await repository.Create();
    }

    private IMongoRepository InitRepository<T>(string name, Func<IMongoCollection<T>, IMongoRepository> ctor)
    {
        var collection = _miraSdb.GetCollection<T>(name);
        IMongoRepository repository = ctor(collection);
        _repositories.Add(name, repository);
        return repository;
    }
}
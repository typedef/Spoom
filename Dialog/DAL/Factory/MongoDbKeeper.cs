using DAL.Contracts;

namespace DAL.Factory;

public static class MongoDbKeeper
{
    private static MongoDbFactory _mongoDbFactory;
    
    public static void Init()
    {
        _mongoDbFactory = new MongoDbFactory();
        _mongoDbFactory.InitRepositories();
    }
    
    public static async Task Deploy()
    {
        _mongoDbFactory = new MongoDbFactory();
        await _mongoDbFactory.RecreateRepositories();
    }

    public static T GetRepository<T>(string repositoryName)
    {
        if (_mongoDbFactory == null)
            throw new Exception("Unitialized Db Keeper");
        
        return (T) _mongoDbFactory.GetRepository(repositoryName);
    }
}
using DAL.Contracts;
using Kernel.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using Kernel.Types;

namespace DAL.Repository;

public class BookRepository : IBookRepository, IMongoRepository
{
    private readonly IMongoCollection<BookModel> _booksCollection;
    
    public BookRepository(IMongoCollection<BookModel> booksCollection)
    {
        _booksCollection = booksCollection;
    }

    public async Task Create()
    {
        var defaultBookModel = new BookModel()
        {
            Id = Guid.NewGuid().ToString("N"),
            Name = "Дефолтная книга",
            ProducerId = Guid.NewGuid().ToString("N"),
            AgeLimit = 0,
            PublishingDate = DateTime.Parse("10.04.2013"),
            PagesCount = 1337,
            Genre = GenreType.Biography,
        };

        var models = new List<BookModel>()
        {
            defaultBookModel
        };

        await _booksCollection.InsertManyAsync(models);
    }

    public async Task<List<BookModel>> GetAllBooksModelsAsync()
    {
        var books = await _booksCollection.Find(new BsonDocument()).ToListAsync();
        return books;
    }

    public async Task AddBookAsync(BookModel bookModel)
    {
        await _booksCollection.InsertOneAsync(bookModel);
    }
    
    public async Task RemoveBookByIdAsync(string id)
    {
        var doc = new BsonDocument("_id", id);
        await _booksCollection.DeleteOneAsync(doc);
    }
    
    public async Task UpdateBookAsync(string id, BookModel newModel)
    {
        await _booksCollection.ReplaceOneAsync(new BsonDocument() {{"_id", id}}, newModel);
    }

#if false
    public async Task<List<BookModel>> Find(BooksFilter bookFilter)
    {
        var builder = Builders<BookModel>.Filter;
        // определяем фильтр - находим все документы, где Name = "Tom"
        //FilterDefinition<BsonDocument> filter = new BsonDocumentFilterDefinition<BsonDocument>();
        //if (request.Name != null)
        FilterDefinition<BookModel>? filter = null;

        if (bookFilter.IsEmpty())
        {
            var res = await _booksCollection.FindAsync(builder.Where(d => d.Id != null));
            return res.ToList();
        }

        if (bookFilter.Id != null)
        {
            filter &= builder.Where(d => d.Id != null && d.Id.Equals(bookFilter.Id.Field));
        }
        
        if (bookFilter.ProducerId != null)
        {
            if (filter == null)
            {
                filter = builder.Where(d => d.ProducerId != null);
            }
            else
            {
                filter &= builder.Where(d => d.ProducerId != null);
            }

            filter &= builder.Where(d => d.ProducerId.Equals(bookFilter.ProducerId.Field));
        }

        if (bookFilter.AgeLimit != null)
        {
            filter &= builder.Where(d => d.AgeLimit.Equals(bookFilter.AgeLimit.Field));
        }
        
        if (bookFilter.PublishingDate != null)
        {
            if (filter == null)
            {
                filter = builder.Where(d => d.PublishingDate != null);
            }
            else
            {
                filter &= builder.Where(d => d.PublishingDate != null);
            }

            if (bookFilter.PublishingDate.Type == CompareFieldType.Full)
            {
                filter &= builder.Where(d => d.PublishingDate.Equals(bookFilter.PublishingDate.Field));
            }
            else if (bookFilter.PublishingDate.Type == CompareFieldType.Greater)
            {
                filter &= builder.Where(d => d.PublishingDate > bookFilter.PublishingDate.Field);
            }
            else if (bookFilter.PublishingDate.Type == CompareFieldType.Less)
            {
                filter &= builder.Where(d => d.PublishingDate < bookFilter.PublishingDate.Field);
            }
        }
        
        if (bookFilter.Genre != null)
        {
            if (filter == null)
                filter = builder.Where(d => d.Genre == bookFilter.Genre.Field);
            else 
                filter &= builder.Where(d => d.Genre == bookFilter.Genre.Field);
        }
        
        if (bookFilter.Name != null)
        {
            if (filter == null)
            {
                filter = builder.Where(d => d.Name != null);
            }
            else
            {
                filter &= builder.Where(d => d.Name != null);
            }
            
            if (bookFilter.Name.Type == CompareFieldType.Full)
            {
                filter &= builder.Where(d => d.Name.Equals(bookFilter.Name.Field));
            }
            else if (bookFilter.Name.Type == CompareFieldType.Contains)
            {
                filter &= builder.Where(d => d.Name.Contains(bookFilter.Name.Field));
            }
            else if (bookFilter.Name.Type == CompareFieldType.StartsWith)
            {
                filter &= builder.Where(d => d.Name.StartsWith(bookFilter.Name.Field));
            }
            else if (bookFilter.Name.Type == CompareFieldType.EndsWith)
            {
                filter &= builder.Where(d => d.Name.EndsWith(bookFilter.Name.Field));
            }
        }

        var result = await _booksCollection.FindAsync(filter);
        List<BookModel> resultList = result.ToList();
        return resultList;
    }
#endif
}
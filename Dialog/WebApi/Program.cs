using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using WebApi.Configuration;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

// Auth with JWT 
builder.Services.AddAuthentication(o =>
{
    o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    o.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
    o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(o => o.TokenValidationParameters = new TokenValidationParameters()
{
    // указывает, будет ли валидироваться издатель при валидации токена
    ValidateIssuer = true,
    // строка, представляющая издателя
    ValidIssuer = JwtConfiguration.Issuer,
    // будет ли валидироваться потребитель токена
    ValidateAudience = true,
    // установка потребителя токена
    ValidAudience = JwtConfiguration.Audience,
    // будет ли валидироваться время существования
    ValidateLifetime = true,
    // установка ключа безопасности
    IssuerSigningKey = JwtConfiguration.KeyHash,
    // валидация ключа безопасности
    ValidateIssuerSigningKey = true,
});

// Add services to the container.
builder.Services.AddControllers()
    .AddJsonOptions(o =>
        o.JsonSerializerOptions.PropertyNamingPolicy = new UpperCamelCasePolicy());
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
// Enable cors
builder.Services.AddCors(o =>
    o.AddPolicy(CorsConfiguration.CorsPolicyName,
        confPolicy =>
        {
            confPolicy.WithOrigins(CorsConfiguration.CorsOrigin)
                .AllowAnyHeader()
                .AllowAnyMethod();
        }));


var app = builder.Build();

app.UseAuthentication();
app.UseAuthorization();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHttpsRedirection();
// Between UserRouting and UseEndpoints
app.UseCors(CorsConfiguration.CorsPolicyName);
app.UseAuthorization();

app.MapControllers();

app.Run();
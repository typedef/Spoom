using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using WebApi.Configuration;

namespace WebApi.Api;

[ApiController]
[EnableCors(CorsConfiguration.CorsPolicyName)]
public class BaseController
{
}
#pragma warning disable CS8604 // Possible null reference argument.

using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Kernel.Data.ViewModels.Auth;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using WebApi.Configuration;
using WebApi.Services.BaseHelpers;
using WebApi.Services.Exceptions;

namespace WebApi.Api;

[ApiController]
[Route("api/user")]
public class UserController : BaseController
{
    // temporary
    private readonly Dictionary<string, UserViewModel> _userTable;

    public UserController()
    {
        _userTable = new Dictionary<string, UserViewModel>()
        {
            { "v", new UserViewModel()
                {
                    Login = "v",
                    Type = UserType.Admin,
                    Password = "123", /*123*/
                }
            },
            { "Vladimir.Efremov@yandex.com", new UserViewModel()
                {
                    Login = "Vladimir.Efremov@yandex.com",
                    Type = UserType.Admin,
                    Password = "123",
                }
            }
        };
    }

    [HttpPost]
    [Route("login")]
    public UserViewModel? Login(LoginRequestViewModel request)
    {
        Guard.NotNull(request);
        Guard.NotNull(request.Login);

        if (!IsUserAlreadyExist(request.Login, request.Password))
            throw new Exception($"User {request.Login} is not registered!");
        
        UserViewModel user = _userTable[request.Login!];
        user.AccessToken = GetJwtToken(request.Login, user.Type.ToString());
        return user;
    }

    [HttpPost]
    [Route("register")]
    public UserViewModel? Register(RegisterRequestViewModel request)
    {
        Guard.NotNull(request);
        Guard.NotNull(request.Login);

        if (IsUserAlreadyExist(request.Login, request.Password))
            throw new UserAlreadyRegisteredException();
        
        var jwt = GetJwtToken(request.Login, request.Type.ToString());

        return null;
    }

    [HttpGet]
    [Authorize]
    [Route("check-access")]
    public bool CheckAccess()
    {
        return true;
    }
    
    [HttpGet]
    [Route("check-access2")]
    public bool CheckAccess2()
    {
        return true;
    }

    private bool IsUserAlreadyExist(string login, string password)
    {
        if (!_userTable.ContainsKey(login))
            return false;

        var val = _userTable[login];
        if (val.Password != password)
            return false;
        
        return true;
    }
    
    private string GetJwtToken(string login, string role)
    {
        
        // payload part
        var claims = new List<Claim>()
        {
            new Claim(ClaimTypes.Email, login),
            new Claim(ClaimTypes.Role, role)
        };

        var cred = new SigningCredentials(JwtConfiguration.KeyHash, SecurityAlgorithms.HmacSha256);

        var token = new JwtSecurityToken(
            issuer: JwtConfiguration.Issuer,
            audience: JwtConfiguration.Audience,
            claims: claims,
            expires: DateTime.Now.AddDays(30),
            signingCredentials: cred
        );

        var jwt = new JwtSecurityTokenHandler().WriteToken(token);
        return jwt;
    }
}
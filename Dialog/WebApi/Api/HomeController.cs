using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Api;

[ApiController]
[Authorize]
[Route("api/home")]
public class HomeController : ControllerBase
{
    private readonly string _backendVersion = "1.0.0";

    [HttpGet]
    [Route("home-get-version")]
    public string GetVersion()
    {
        return _backendVersion;
    }
}
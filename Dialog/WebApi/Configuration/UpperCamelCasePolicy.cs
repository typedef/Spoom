using System.Text.Json;

namespace WebApi.Configuration;

public class UpperCamelCasePolicy: JsonNamingPolicy
{
    public override string ConvertName(string name)
    {
        var str = name.ToCharArray();
        str[0] = char.ToUpper(str[0]);
        return new string(str);
    }
}
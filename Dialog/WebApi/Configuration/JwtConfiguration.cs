using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace WebApi.Configuration;

public class JwtConfiguration
{
    public const string Issuer = "DialogWebServer" + "f3D7@jFdlA_1s+-";
    public const string Audience = "DialogWebClient" + "dE_0sw@!sol813*(HJ)IDW_{";
    public const string Key = "MakeThisKeyWayBiggerThanItIsNow" + Issuer + Audience;

    public static SymmetricSecurityKey KeyHash
    {
        get
        {
            if (_keyHash == null)
                _keyHash = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Key));
            return _keyHash;
        }
    }

    private static SymmetricSecurityKey _keyHash;
}
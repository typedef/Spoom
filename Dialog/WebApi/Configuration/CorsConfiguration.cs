namespace WebApi.Configuration;

public static class CorsConfiguration
{
    public const string CorsPolicyName = "DialogCorsPolicy";
    public const string CorsOrigin = "http://localhost:4200";
}